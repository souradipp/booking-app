import express from 'express';
const router = express.Router();

import { showMsg } from '../controllers/authController'

router.get("/", showMsg);

module.exports = router;