// const express = require('express');
import express from 'express';
import fs from "fs"
const morgan = require('morgan');
require('dotenv').config()
const app = express();

app.use(morgan("dev"))

fs.readdirSync("./routes").map((route) =>
    app.use("/api", require(`./routes/${route}`)
    ))

const PORT = process.env.PORT;
app.listen(PORT, () => { console.log("Server started") })