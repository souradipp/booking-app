
export const authReducer = (state = {}, action) => {
    switch (action.type) {
        case "LOGGED_IN":
            return { ...state, ...action.payload };
        case "LOGOUT":
            return { ...state };
        default:
            return state;
    }
}