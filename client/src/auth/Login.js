import React from 'react'
import { useSelector } from 'react-redux'

const Login = () => {
    const { user } = useSelector((state) => ({ ...state }));

    console.log("state ", user)
    return (
        <div className="container-fluid h1 p-5 text-center">
            Login page
        </div>
    )
}

export default Login
