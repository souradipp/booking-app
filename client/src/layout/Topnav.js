import React from 'react'
import { Link } from 'react-router-dom'

const navMenu = [
    { title: "Login", path: "/" },
    { title: "Register", path: "/register" },
    { title: "Home", path: "/home" },
]

const Topnav = () => {

    const displayNavBar = () => (
        navMenu.map((menu, index) => {
            return (
                <Link key={index} to={menu.path} className="nav-link">{menu.title}</Link>
            )
        })
    )

    return (
        <div className="nav bg-light d-flex justify-content-between">
            {displayNavBar()}
        </div>
    )
}

export default Topnav
