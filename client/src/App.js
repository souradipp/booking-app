
import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from './booking/Home'
import Login from './auth/Login'
import Register from './auth/Register'
import Topnav from './layout/Topnav'



function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Topnav />
        <Switch>
          <Route path="/register" exact={true} component={Register} />
          <Route path="/home" exact={true} component={Home} />
          <Route path="/" component={Login} />

        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
